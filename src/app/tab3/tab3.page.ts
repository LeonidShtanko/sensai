import { Component } from '@angular/core';
import { DeviceMotion, DeviceMotionAccelerationData, DeviceMotionAccelerometerOptions } from '@ionic-native/device-motion/ngx';
import { CameraPreview, CameraPreviewPictureOptions, CameraPreviewOptions, CameraPreviewDimensions } from '@ionic-native/camera-preview/ngx';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  currentImage: any;
  picture: any;
  x: any;
  y: any;
  z: any;
  subscription: any;

  constructor(
    private deviceMotion: DeviceMotion,
    private cameraPreview: CameraPreview,
  ) { }

  OpenPreview() {
    const cameraPreviewOpts: CameraPreviewOptions = {
      x: 0,
      y: 0,
      width: window.screen.width,
      height: window.screen.height,
      tapPhoto: false,
      toBack: false,
      alpha: 1,
    }
    this.currentImage = this.cameraPreview.startCamera(cameraPreviewOpts);

    const pictureOpts: CameraPreviewPictureOptions = {
      width: window.screen.width,
      height: window.screen.height,
      quality: 85
    }

    setInterval(() => {
      this.cameraPreview.takeSnapshot(pictureOpts).then((imageData) => {
        this.picture = 'data:image/jpeg;base64,' + imageData;});
    }, 33);

  }


  start() {
    const option: DeviceMotionAccelerometerOptions = {
      frequency: 100
    }
    this.subscription = this.deviceMotion.watchAcceleration(option).subscribe((acceleration: DeviceMotionAccelerationData) => {
      this.x = Math.round(((acceleration.x) / 9.81) * 90);
      this.y = Math.round(((acceleration.y) / 9.81) * 90);
      this.z = Math.round(((acceleration.z) / 9.81) * 90);
    });
  }

  stop() {
    this.subscription.unsubscribe();
  }
}
